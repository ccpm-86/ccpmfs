#!/bin/sh

hexdump -C $1 | cut -d ' '  -f 1 | cut -c 1-5 | grep -v '*' | uniq -c
