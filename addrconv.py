def conv(n, l, base=0):
    if n < l // 2:
        result = (n//2)*4 + (n%2)
    else:
        result = l - 2 - ((n-l//2)//2)*4 + (n%2)
    return base + result

def print_hex(l, base=0):
    for n in range(l):
        if n == l // 2:
            print()
        print(" {:02x}".format(conv(n, l, base, shift)), end="")
    print()

# fd params: length=156, base=4
for n in range(1, 126):
    print("{:03} {:02x}".format(n, conv(n, 156, 4) * 2048))
