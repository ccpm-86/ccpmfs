#!/usr/bin/env python

import sys
import types
import os.path

def array_to_blocks(array, addr_bytes):
    result = []
    i = 0
    while i < len(array):
        sa = list(array[i:i+addr_bytes])
        if sa != [0] * len(sa):
            l = list(array[i:i+addr_bytes])
            n = 0
            for x in reversed(l):
                n = n * 256 + x
            result.append(n)
        i += addr_bytes
    return result

def block_to_addr_fd(fs, block):
    def conv(n, l):
        if n < l // 2:
            result = (n//2)*4 + (n%2)
        else:
            result = l - 2 - ((n-l//2)//2)*4 + (n%2)
        return result
    n_blocks = fs.data_blocks - 1     # works with "-1", don't know why yet
    return fs.dir_start + (conv(block, n_blocks) * fs.block_size)

def block_to_addr_hd(fs, block):
    return fs.dir_start + (block * fs.block_size)

def to_function(function_name):
    possibles = globals().copy()
    possibles.update(locals())
    function = possibles.get(function_name)
    if not function:
        raise NotImplementedError("Method %s not implemented" % function_name)
    return function

def call(function, arguments):
    return function(*arguments)

def file_write(name, data):
    f = open(name, "wb")
    f.write(data)
    print("written {} bytes to {}".format(len(data), name))

def copy(fs, filename, outputname=None):
    if outputname is None:
        outputname = filename
    print("copy: {} to {}".format(filename, outputname))
    step = 32
    i = fs.dir_start
    file_data = bytearray()
    while fs.data[i+1] < 0x80:
        # TODO: abstract elements in common with dirlst
        name0 = fs.data[i+1:i+12]
        name1 = "".join(map(lambda c: chr(c & 0x7f), name0))
        name = (name1[:-3].strip() + "." + name1[-3:].strip()).lower()
        rc = fs.data[i+15]
        blocks = array_to_blocks(fs.data[i+16:i+32], fs.addr_bytes)
        nblocks = len(blocks) # count_blocks(fs.data[i+16:i+32], addr_bytes)
        blk_k = fs.block_size // 1024
        B = (nblocks*blk_k - 1) // 16    # ceiling (a / b) - 1 = (a - 1) // b
        records = rc + 128 * B
        sum_recs = 0
        if name == filename:
            print("directory entry {}".format(i//32))
            for (i,block) in enumerate(blocks):
                addr = fs.block_to_addr(fs, block)
                if i < len(blocks) - 1:
                    recs = fs.block_size // 128
                else:
                    recs = rc % (fs.block_size // 128)
                sum_recs += recs
                block_data = fs.data[addr:addr+recs*128]
                file_data += block_data
                print("    block {:02x}: address={:05x}, records={}".format(block, addr, recs))
            print("total records: {}, sum_recs: {}".format(records, sum_recs))
        i += step
    print("l={}".format(len(file_data)))
    if len(file_data) > 0:
        file_write(outputname, file_data)

def name_large(name):
    name1 = "".join(map(lambda c: chr(c & 0x7f), name))
    return name1[:-3] + " " + name1[-3:]

def name_simple(name):
    name1 = ("".join(map(lambda c: chr(c & 0x7f), name))).lower()
    n = name1[0:8].strip()
    t = name1[8:12].strip()
    return "{}.{}".format(n, t)

def dirlst(fs):
    step = 32
    i = fs.dir_start
    while fs.data[i+1] < 0x80:
        name = name_large(fs.data[i+1:i+12])
        ex = fs.data[i+12]
        s2 = fs.data[i+14]
        rc = fs.data[i+15]
        entry = ((32*s2)+ex) // (fs.dpb.exm+1)
        blk_k = fs.block_size // 1024
        records = rc + 128 * (ex & fs.dpb.exm)
        nblocks = (records - 1) // 2**fs.dpb.bsh + 1
        sz_k = nblocks * blk_k
        print("{:05x}:{:14} entry={} blocks={} rc={} size={}K recs={}"
              .format(i, name, entry, nblocks, rc, sz_k, records))
        i += step

def ls1(fs):
    step = 32
    i = fs.dir_start
    names = set()
    while fs.data[i+1] < 0x80:
        name = name_simple(fs.data[i+1:i+12])
        if not name in names:
            print(name)
            names.add(name)
        i += step

def info(fs):
    if len(fs.data) == 327680: # fd
        print("fd system")
    elif len(fs.data) == 10653696: #hd
        print("hd system")
    print("blocks_on_disk: {}, addr_bytes={}".format(fs.blocks_on_disk, fs.addr_bytes))
    print("block_size = 0x{:02x} ({})".format(fs.block_size, fs.block_size))
    print("sector_size = 0x{:02x} ({})".format(fs.sector_size, fs.sector_size))
    print("reserved_tracks = 0x{:02x} ({})".format(fs.dpb.off, fs.dpb.off))
    print("reserved_bytes = 0x{:x} ({})".format(fs.reserved_bytes, fs.reserved_bytes))
    print("track_size = 0x{:02x} ({})".format(fs.track_size, fs.track_size))
    print("dir_start = 0x{:02x} ({})".format(fs.dir_start, fs.dir_start))
    print("dir_blocks: {}, data_blocks = {}".format(fs.dir_blocks, fs.data_blocks))
    print("file_offset = 0x{:x} ({})".format(fs.file_offset, fs.file_offset))


def main(filename, command, args):

    # Initialize data
    fs = types.SimpleNamespace()

    # Open the file
    f_in = open(filename, "rb")

    # Read the file
    fs.data = f_in.read()
    f_in.close()

    # Initialize parameters
    if len(fs.data) == 327680: # fd
        fs.disk_start = 0x1000 # why?
        fs.block_to_addr = block_to_addr_fd
        fs.dpb = types.SimpleNamespace()
        fs.dpb.spt = 0x0008
        fs.dpb.bsh = 0x04
        fs.dpb.blm = 0x0f
        fs.dpb.exm = 0x01
        fs.dpb.dsm = 0x009d
        fs.dpb.drm = 0x003f
        fs.dpb.al0 = 0x80
        fs.dpb.al1 = 0x00
        fs.dpb.cks = 0x0010
        fs.dpb.off = 0x0001
        fs.dpb.psh = 0x02
        fs.dpb.prm = 0x03
    elif len(fs.data) == 10653696: #hd
        fs.disk_start = 0
        fs.block_to_addr = block_to_addr_hd
        fs.dpb = types.SimpleNamespace()
        fs.dpb.spt = 0x0011
        fs.dpb.bsh = 0x05
        fs.dpb.blm = 0x1f
        fs.dpb.exm = 0x01
        fs.dpb.dsm = 0x07ff
        fs.dpb.drm = 0x07ff
        fs.dpb.al0 = 0xff
        fs.dpb.al1 = 0xff
        fs.dpb.cks = 0x8000
        fs.dpb.off = 0x0001
        fs.dpb.psh = 0x04
        fs.dpb.prm = 0x0f

    # Get more info
    fs.blocks_on_disk = fs.dpb.dsm + 1 # without track offset
    fs.addr_bytes = 1 if fs.blocks_on_disk < 256 else 2  # block reference size in directory

    fs.block_size = 2**(fs.dpb.bsh+7)
    fs.sector_size = 2**(fs.dpb.psh+7)
    fs.track_size = fs.dpb.spt * fs.sector_size
    fs.reserved_bytes = fs.dpb.off * fs.track_size
    fs.dir_start = fs.disk_start + fs.reserved_bytes
    fs.dir_blocks = ((fs.dpb.drm+1) * 32 + fs.block_size - 1) // fs.block_size
    fs.file_offset = fs.dir_start + fs.dir_blocks * fs.block_size - fs.block_size # blocks start at 1
    fs.data_blocks = fs.blocks_on_disk - fs.dir_blocks

    # call the function
    function = to_function(command)
    call(function, [fs] + args)

is_cli = False
if __name__ == "__main__" :
    is_cli = True

if not is_cli:
    print("Enter file name: ", end="")
    filename = sys.stdin.readline().strip()
    print("Enter the command and arguments: ", end="")
    tmp = sys.stdin.readline().strip().split()
    command = tmp[0]
    args = tmp[1:]
else:
    if len(sys.argv) < 3:
        print("Usage {} <image> [<command>]".format(sys.argv[0]))
        exit()
    filename = sys.argv[1]
    command = sys.argv[2]
    args = sys.argv[3:]

if not os.path.isfile(filename):
    print("The file \"{}\" does not exist.".format(filename))
    exit()

print("filename = \"{}\"".format(filename))
print("command = {}".format(command))
print("arguments = {}".format(args))

main(filename, command, args)
